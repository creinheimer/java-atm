package com.atm.service;

import com.atm.exception.ValueNotSupportedException;
import com.atm.model.enums.BillValue;
import static com.atm.model.enums.BillValue.*;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class ATMServiceTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testCashOut_zero_value() {
		System.out.println("cashOut");
		Integer value = 0;
		thrown.expect(IllegalArgumentException.class);
		ATMService instance = new ATMService();
		instance.cashOut(value);
	}

	@Test
	public void testCashOut_invalid_value() {
		System.out.println("cashOut");
		Integer value = 17;
		thrown.expect(ValueNotSupportedException.class);
		ATMService instance = new ATMService();
		instance.cashOut(value);
	}

	@Test
	public void testCashOut_270_value() {
		System.out.println("cashOut");
		Integer value = 270;
		ATMService instance = new ATMService();
		List<BillValue> expResult = Arrays.asList(BILL_100, BILL_100, BILL_50, BILL_20);
		List<BillValue> result = instance.cashOut(value);
		assertEquals(expResult, result);
	}

	@Test
	public void testCashOut_30_value() {
		System.out.println("cashOut");
		Integer value = 30;
		ATMService instance = new ATMService();
		List<BillValue> expResult = Arrays.asList(BILL_20, BILL_10);
		List<BillValue> result = instance.cashOut(value);
		assertEquals(expResult, result);
	}

}
