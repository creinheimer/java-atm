package com.atm.service;

import com.atm.exception.ValueNotSupportedException;
import com.atm.model.enums.BillValue;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class ATMService {
	
	private final List<BillValue> BILLS_VALUE = Arrays.asList(BillValue.values());
	
	public List<BillValue> cashOut(int value){

		if (value == 0) {
			throw new IllegalArgumentException("O valor de saque deve ser maior que zero.");
		}
		
		Deque<BillValue> availableBills = new ArrayDeque<>(BILLS_VALUE);
		List<BillValue> cashOutBills = new ArrayList<>();

		while (value != 0 && !availableBills.isEmpty()) {
			int aux = value - availableBills.peek().getValue();
			if (aux >= 0 && value >= availableBills.peek().getValue()) {
				value = value - availableBills.peek().getValue();
				cashOutBills.add(availableBills.peek());
			} else {
				availableBills.pop();
			}
		}
		
		if (value > 0) {
			throw new ValueNotSupportedException("Notas não comportam este valor.");
		}

		return cashOutBills;
		
	}
	
}
