package com.atm.exception;

public class ValueNotSupportedException extends RuntimeException {

	public ValueNotSupportedException(String string) {
		super(string);
	}
	
}
