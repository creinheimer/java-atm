package com.atm.model.enums;

public enum BillValue {

	BILL_100(100), BILL_50(50), BILL_20(20), BILL_10(10);

	private final int value;

	private BillValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}
